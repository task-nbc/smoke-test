package Test;

import driver.DriverManager;
import driver.DriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static navigations.ApplicationURLs.SWAG_LABS_URL;


public class BaseTest {


    @BeforeMethod
    public void setup() {
        DriverManager.getDriver();
        DriverUtils.setInitialConfiguration();
        DriverUtils.navigateToPage(SWAG_LABS_URL);
    }

    @AfterMethod
    public void tearDown() {
        DriverManager.disposeDriver();
    }
}
